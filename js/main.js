$(document).ready(function(){
    $('[data-toggle="mobileFriendlyTooltip"]').tooltip(); 
});

function changeMe() {
    var src = $("#headshot").attr('src');
    if(src === "img/head.png") {
        $("#headshot").attr("src","img/head2.png");
    } else {
        $("#headshot").attr("src","img/head.png");
    }
}

function changeBgColor(){
    if($("body").css("background-color") === "rgb(189, 189, 189)") {
        $("body").css("background-color","#fff");
        $("#main-container").css("background-color","#fff");
    } else {
        $("body").css("background-color","#BDBDBD");
        $("#main-container").css("background-color","#BDBDBD");
    }
}